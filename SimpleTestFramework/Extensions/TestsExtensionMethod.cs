﻿namespace SimpleTestFramework.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ComponentModel.Composition.Hosting;
    using System.ComponentModel.Composition;

    public static class TestsExtensionMethod
    {
        public static IEnumerable<Action> Tests(this object obj)
        {
            IEnumerable<Action> results = new List<Action>();
            using (var cat = new TypeCatalog(obj.GetType()))
            {
                var container = new CompositionContainer(cat);
                results = container.GetExportedValues<Action>("Behavior");
            }

            return results;
        }

        //untested
        public static Action Setup(this object obj)
        {
            return GetExport(obj.GetType(), "BeforeEachTest");
        }

        //untested
        public static Action Teardown(this object obj)
        {
            return GetExport(obj.GetType(), "AfterEachTest");
        }

        //untested
        private static Action GetExport(Type obj, string contract)
        {
            Action result = null;
            using (var cat = new TypeCatalog(obj))
            {
                try
                {
                    var container = new CompositionContainer(cat);
                    result = container.GetExportedValue<Action>(contract);
                }
                catch (ImportCardinalityMismatchException)
                {

                }
            }
            return result;
        }
    }
}
