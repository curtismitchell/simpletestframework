﻿using System;
using System.Linq;
using System.ComponentModel.Composition;

namespace SimpleTestFramework
{
    [AttributeUsage(validOn:AttributeTargets.Method)]
    public class BehaviorAttribute : ExportAttribute
    {
        public BehaviorAttribute() : base("Behavior", typeof(Action))
        {

        }
    }
}
