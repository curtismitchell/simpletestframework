﻿namespace SimpleTestFramework
{
    using System;
    using System.ComponentModel.Composition;

    [AttributeUsage(validOn:AttributeTargets.Method,AllowMultiple=false)]
    public class BeforeEachTestAttribute : ExportAttribute
    {
        public BeforeEachTestAttribute() : base("BeforeEachTest")
        {

        }
    }
}
