﻿namespace SimpleTestFramework
{
    using System;

    public static class Assert
    {
        public static event EventHandler<AssertCalledEventArgs> AssertCalled;

        public static void That(bool condition)
        {
            if (!condition)
            {
                if (null != AssertCalled) AssertCalled(null, new AssertCalledEventArgs() { Passed = false });
                throw new AssertionException();
            }
            else
            {
               if (null != AssertCalled) AssertCalled(null, new AssertCalledEventArgs() { Passed = true });
            } 
        }
    }

    public class AssertCalledEventArgs : EventArgs
    {
        public bool Passed { get; set; }
    }
}

//TODO: Debug the runner as it tests itself. Pass the test libs to it via the command line in the build events