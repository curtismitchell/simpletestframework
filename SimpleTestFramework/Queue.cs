﻿namespace SimpleTestFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.ComponentModel.Composition.Hosting;

    public class Queue
    {
        public Queue(Assembly assembly)
        {
            Load(assembly);
        }

        public void Load(Assembly assembly)
        {
            using (var cat = new AssemblyCatalog(assembly))
            {
                var container = new CompositionContainer(cat);
                Units = container.GetExportedValues<object>("TestUnit");
            }
        }

        public IEnumerable<object> Units { get; private set; }
    }
}
