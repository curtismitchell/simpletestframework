﻿namespace SimpleTestFramework
{
    using System;

    public class AssertionException : Exception
    {
        public AssertionException() : base("The assertion failed.")
        {

        }
    }
}
