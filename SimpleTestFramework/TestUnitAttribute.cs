﻿namespace SimpleTestFramework
{
    using System;
    using System.ComponentModel.Composition;

    [AttributeUsage(AttributeTargets.Class)]
    public class TestUnitAttribute : ExportAttribute 
    {
        public TestUnitAttribute() : base("TestUnit")
        {

        }
    }
}
