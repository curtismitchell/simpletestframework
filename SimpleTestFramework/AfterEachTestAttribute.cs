﻿namespace SimpleTestFramework
{
    using System;
    using System.ComponentModel.Composition;

    [AttributeUsage(validOn:AttributeTargets.Method)]
    public class AfterEachTestAttribute : ExportAttribute
    {
        public AfterEachTestAttribute() : base("AfterEachTest")
        {

        }
    }
}
