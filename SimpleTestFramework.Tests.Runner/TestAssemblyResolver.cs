﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleTestFramework.Tests.Runner
{
    using System.Reflection;
    using SimpleTestFramework.Runner;

    class TestAssemblyResolver : IAssemblyResolver
    {
        public Assembly ResolveAssembly(string name)
        {
            if (name == "NotALibrary.dll") return null;

            return Assembly.Load(global::SimpleTestFramework.Tests.Runner.Properties.Resources.LibraryWithTests);
        }
    }
}
