﻿namespace SimpleTestFramework.Tests.Runner
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SimpleTestFramework;
    using SimpleTestFramework.Runner;

    [TestUnit]
    public class TestRunner
    {
        private readonly string goodLibPath = @"..\TestDLL\LibraryWithTests.dll";
        IAssemblyResolver testResolver;
        Runner runner;
        List<string> asms;

        [BeforeEachTest]
        public void Before_Each_Test()
        {
            testResolver = new TestAssemblyResolver();
            asms = new List<string>() { goodLibPath };
            runner = new Runner(asms) { AssemblyResolver = testResolver };
        }

        [Behavior]
        public void Should_Skip_Assemblies_That_Cannot_Be_Found()
        {
            asms.Add("NotALibrary.dll");

            runner.AssemblySkipped += (s, e) =>
            {
                Assert.That("NotALibrary.dll" == e.AssemblyName);
            };
            runner.Run();
        }

        [Behavior]
        public void Should_Resolve_Relative_Paths_Of_Assemblies()
        {
            runner.Run();
            Assert.That(2 == runner.TotalTestsRun);
        }

        [Behavior]
        public void Should_Run_Test_For_Found_Assemblies()
        {
            runner.Run();
            Assert.That(2 == runner.TotalTestsRun);
        }

        [Behavior]
        public void Should_Return_Number_Of_Assertions()
        {
            runner.Run();
            Assert.That(2 == runner.AssertionsCalled);
        }

        [Behavior]
        public void Should_Return_Number_Of_Assertions_That_Passed()
        {
            runner.Run();
            Assert.That(1 == runner.AssertionsPassed);
        }

        [Behavior]
        public void Should_Return_Number_Of_Passed_Tests()
        {
            runner.Run();
            Assert.That(1 == runner.TotalPassingTests);
        }

        [Behavior]
        public void Should_Return_Number_Of_Failed_Tests()
        {
            runner.Run();

            Assert.That(1 == runner.TotalFailedTests);
        }

        [Behavior]
        public void Should_Fire_Event_When_Each_Test_Completes()
        {
            var testsCompleted = 0;

            runner.TestCompleted += (s, e) =>
            {
                testsCompleted++;
            };
            runner.Run();
            Assert.That(runner.TotalTestsRun == testsCompleted);
        }
    }
}
