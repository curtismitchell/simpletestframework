﻿namespace RunnerTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SimpleTestFramework.Runner;
    
    [TestClass]
    public class RunnerTests
    {
        List<string> asms;
        Runner runner;

        [TestInitialize]
        public void Before_Each_Test()
        {
            asms = new List<string>() { @"..\..\..\TestDLL\LibraryWithTests.dll" };
            
            runner = new Runner(asms);
            runner.AssemblyResolver = new TestAssemblyResolver();
        }

        [TestMethod]
        public void Should_Skip_Assemblies_That_Cannot_Be_Found()
        {
            asms.Add("NotALibrary.dll");
            runner = new Runner(asms);
            
            runner.AssemblySkipped += (s, e) =>
            {
                Assert.AreEqual("NotALibrary.dll", e.AssemblyName);
            };
            runner.Run();
        }

        [TestMethod]
        public void Should_Resolve_Relative_Paths_Of_Assemblies()
        {
            asms.Clear();
            asms.Add(@"..\..\..\TestDLL\LibraryWithTests.dll");
            runner = new Runner(asms);

            runner.Run();
            Assert.AreEqual(2, runner.TotalTestsRun);
        }

        [TestMethod]
        public void Should_Run_Test_For_Found_Assemblies()
        {
            runner.Run();
            Assert.AreEqual(2, runner.TotalTestsRun);
        }

        [TestMethod]
        public void Should_Return_Number_Of_Assertions()
        {
            runner.Run();
            Assert.AreEqual(2, runner.AssertionsCalled);
        }

        [TestMethod]
        public void Should_Return_Number_Of_Passed_Tests()
        {
            runner.Run();
            Assert.AreEqual(1, runner.TotalPassingTests);
        }

        [TestMethod]
        public void Should_Return_Number_Of_Failed_Tests()
        {
            runner.Run();
            Assert.AreEqual(1, runner.TotalFailedTests);
        }

        [TestMethod]
        public void Should_Fire_Event_When_Each_Test_Completes()
        {
            var testsCompleted = 0;

            runner.TestCompleted += (s, e) =>
            {
                testsCompleted++;
            };
            runner.Run();
            Assert.AreEqual(runner.TotalTestsRun, testsCompleted);
        }
    }
}
