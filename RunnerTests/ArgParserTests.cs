﻿namespace RunnerTests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SimpleTestFramework.Runner;

    [TestClass]
    public class ArgParserTests
    {
        [TestMethod]
        public void Should_Accept_A_Library_As_An_Argument()
        {
            string[] args = "/lib:test.dll".Split(' ');
            var ap = new ArgumentParser(args);
            Assert.AreEqual(1, ap.AssemblyNames.Count());
        }

        [TestMethod]
        public void Should_Accept_Many_Libraries_As_Arguments()
        {
            string[] args = "/lib:test.dll /lib:test2.dll".Split(' ');
            var ap = new ArgumentParser(args);
            Assert.AreEqual(2, ap.AssemblyNames.Count());
        }

        [TestMethod]
        public void Should_Assume_Library_If_Lib_Flag_Omitted()
        {
            string[] args = "test.dll".Split(' ');
            var ap = new ArgumentParser(args);
            Assert.AreEqual("test.dll", ap.AssemblyNames.First());
        }

        [TestMethod]
        public void Should_Parse_Library_Name_From_Argument()
        {
            string[] args = "/lib:testing123.dll /lib:test2abc.dll".Split(' ');
            var ap = new ArgumentParser(args);
            Assert.IsTrue(ap.AssemblyNames.Contains("testing123.dll"));
            Assert.IsTrue(ap.AssemblyNames.Contains("test2abc.dll"));
        }
    }
}
