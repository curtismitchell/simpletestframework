﻿namespace RunnerTests
{
    using System.Reflection;
    using SimpleTestFramework.Runner;

    class TestAssemblyResolver : IAssemblyResolver
    {
        public Assembly ResolveAssembly(string name)
        {
            if (name == "NotALibrary.dll") return null;

            return Assembly.Load(global::RunnerTests.Properties.Resources.LibraryWithTests);
        }
    }
}
