﻿namespace MSTests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using STF = SimpleTestFramework;
    using SimpleTestFramework.Extensions;
    using System.Reflection;

    [TestClass]
    public class QueueTests
    {
        STF.Queue q;

        [TestInitialize]
        public void Before_Each_Test()
        {
            var asm = Assembly.Load(global::MSTests.Properties.Resources.LibraryWithTests);
            q = new STF.Queue(asm);
        }

        [TestMethod]
        public void Should_Recognize_Public_Classes_Flagged_With_The_TestUnit_Attribute()
        {
            Assert.IsTrue(q.Units.Count() == 1);
        }

        [TestMethod]
        public void Should_List_Tests_In_Each_TestUnit()
        {
            var type = q.Units.First();
            Assert.IsTrue(type.Tests().Count() > 0);
        }
    }
}
