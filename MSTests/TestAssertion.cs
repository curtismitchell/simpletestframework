﻿namespace MSTests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using STF = SimpleTestFramework;

    [TestClass]
    public class TestAssertion
    {
        [TestMethod]
        [ExpectedException(typeof(STF.AssertionException))]
        public void Should_Throw_If_Parameter_Is_False()
        {
            SimpleTestFramework.Assert.That(false);
        }

        [TestMethod]
        public void Assert_Should_Fire_Event_When_Called()
        {
            var eventCalled = false;

            STF.Assert.AssertCalled += ((s, e) =>
                {
                    eventCalled = true;
                });
            if(!eventCalled) STF.Assert.That(true);
            Assert.IsTrue(eventCalled);
        }
    }
}
