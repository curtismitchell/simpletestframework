﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using STF = SimpleTestFramework;

namespace MSTests
{
    [TestClass]
    public class TestAttributes
    {
        [TestMethod]
        public void TestUnitAttribute_Should_Be_An_Attribute()
        {
            var tca = new STF.TestUnitAttribute();
            Assert.IsTrue(tca is Attribute);
        }

        [TestMethod]
        public void TestUnitAttribute_Should_Only_Apply_To_Classes()
        {
            var t = typeof(STF.TestUnitAttribute);
            var usageAttribute = t.GetCustomAttributes(typeof(AttributeUsageAttribute), false).SingleOrDefault();
            Assert.IsTrue((usageAttribute as AttributeUsageAttribute).ValidOn == AttributeTargets.Class);
        }

        [TestMethod]
        public void BehaviorAttribute_Should_Be_An_Attribute()
        {
            var b = new STF.BehaviorAttribute();
            Assert.IsTrue(b is Attribute);
        }

        [TestMethod]
        public void BehaviorAttribute_Should_Only_Apply_To_Methods()
        {
            var t = typeof(STF.BehaviorAttribute);
            var usageAttribute = t.GetCustomAttributes(typeof(AttributeUsageAttribute), false).SingleOrDefault();
            Assert.IsTrue((usageAttribute as AttributeUsageAttribute).ValidOn == AttributeTargets.Method);
        }

        [TestMethod]
        public void BehaviorAttribute_Should_Export_A_Contract_Named_Behavior()
        {
            var attr = new STF.BehaviorAttribute();
            Assert.AreEqual("Behavior", attr.ContractName);
        }
    }
}
