﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleTestFramework;

namespace LibraryWithTests
{
    [TestUnit]
    public class SomeTestClass
    {
        [Behavior]
        public void TestThis()
        {
            Assert.That(true);
        }

        [Behavior]
        public void TestFails()
        {
            Assert.That(false);
        }
    }
}
