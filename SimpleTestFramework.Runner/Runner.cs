﻿namespace SimpleTestFramework.Runner
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using SimpleTestFramework.Extensions;

    public class Runner
    {
        readonly IEnumerable<string> assemblyNames;
        public event EventHandler<AssemblySkippedEventArgs> AssemblySkipped;
        public event EventHandler<TestCompletedEventArgs> TestCompleted;

        public int TotalTestsRun { get; private set; }
        public int AssertionsCalled { get; private set; }
        public int TotalPassingTests { get; private set; }
        public int TotalFailedTests { get; private set; }
        public int AssertionsPassed { get; private set; }

        public IAssemblyResolver AssemblyResolver { get; set; }

        public Runner(IEnumerable<string> assemblyNames)
        {
            this.assemblyNames = assemblyNames;
            Assert.AssertCalled += (s, e) =>
            {
                AssertionsCalled++;
                if (e.Passed)
                    AssertionsPassed++;
            };
        }

        private Assembly GetAssembly(string assemblyName)
        {
            if (AssemblyResolver == null) AssemblyResolver = new FileSystemAssemblyResolver();
            return AssemblyResolver.ResolveAssembly(assemblyName);
        }

        private void RunEachUnit(IEnumerable<StagedUnit> units)
        {
            foreach (var u in units)
            {
                foreach (var t in u.Tests)
                {
                    var instance = Activator.CreateInstance(u.Unit);

                    var tce = new TestCompletedEventArgs
                    {
                        TestName = t.Method.Name,
                        Assembly = u.Unit.Name
                    };

                    try
                    {
                        if (u.Setup != null) u.Setup.Method.Invoke(instance, null);
                        t.Method.Invoke(instance, null);
                        TotalPassingTests++;
                    }
                    catch (TargetInvocationException e)
                    {
                        TotalFailedTests++;
                        tce.Exception = e.InnerException;
                    }
                    catch (Exception e)
                    {
                        TotalFailedTests++;
                        tce.Exception = e;
                    }
                    finally
                    {
                        TotalTestsRun++;
                        if (u.TearDown != null) u.TearDown.Method.Invoke(instance, null);
                        if (TestCompleted != null) TestCompleted(u, tce);
                    }
                }
            }
        }

        private IEnumerable<StagedUnit> StageAssembly(Assembly asm)
        {
            var queue = new Queue(asm);
            var results = new List<StagedUnit>();

            foreach (var u in queue.Units)
            {
                Type type = u.GetType();
                results.Add( new StagedUnit(type)
                {
                    Setup = u.Setup(),
                    TearDown = u.Teardown(),
                    Tests = u.Tests()
                } );
            }

            return results;
        }

        public void Run()
        {
            foreach (var s in assemblyNames)
            {
                var asm = GetAssembly(s);
                if (asm == null)
                {
                    if (AssemblySkipped != null) AssemblySkipped(this, new AssemblySkippedEventArgs { AssemblyName = s });
                    continue;
                }

                var units = StageAssembly(asm);

                RunEachUnit(units);
            }
        }
    }

    public class AssemblySkippedEventArgs : EventArgs
    {
        public string AssemblyName { get; set; }
    }

    public class TestCompletedEventArgs : EventArgs
    {
        public string TestName { get; set; }
        public string Assembly { get; set; }
        public Exception Exception { get; set; }
    }

    internal class StagedUnit
    {
        public Action Setup { get; set; }
        public Action TearDown { get; set; }
        public IEnumerable<Action> Tests { get; set; }
        public Type Unit { get; set; }

        public StagedUnit(Type type)
        {
            Unit = type;
        }
    }
}
