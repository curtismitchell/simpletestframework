﻿namespace SimpleTestFramework.Runner
{
    using System.Reflection;
    using System.IO;
    using System;

    class FileSystemAssemblyResolver : IAssemblyResolver
    {
        public Assembly ResolveAssembly(string name)
        {
            // first check to see if the path is absolute
            var path = Path.GetFullPath(name);
            if (File.Exists(path))
                return Assembly.LoadFile(path);

            path = Path.Combine(Environment.CurrentDirectory, name);
            if (File.Exists(path))
                return Assembly.LoadFile(path);

            return null;            
        }
    }
}
