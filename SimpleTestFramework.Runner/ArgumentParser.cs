﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleTestFramework.Runner
{
    public class ArgumentParser
    {
        public ArgumentParser(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                var temp = args[i];
                if (temp.StartsWith("/lib:")) args[i] = temp.Substring("/lib:".Length);
            }
                
            AssemblyNames = new List<string>(args);
        }

        public IEnumerable<string> AssemblyNames { get; private set; }
    }
}
