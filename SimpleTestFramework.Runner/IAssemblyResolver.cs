﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace SimpleTestFramework.Runner
{
    public interface IAssemblyResolver
    {
        Assembly ResolveAssembly(string name);
    }
}
