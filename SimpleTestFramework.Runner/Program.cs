﻿using System;
using System.Linq;

namespace SimpleTestFramework.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var argParser = new ArgumentParser(args);
            if (argParser.AssemblyNames.Count() == 0)
            {
                Console.WriteLine("Usage: SimpleTestFramework.Runner.exe /lib:assembly1.dll /lib:assembly2.dll ...");
                return;
            }

            var runner = new Runner(argParser.AssemblyNames);
            runner.TestCompleted += new EventHandler<TestCompletedEventArgs>(runner_TestCompleted);
            runner.AssemblySkipped += new EventHandler<AssemblySkippedEventArgs>(runner_AssemblySkipped);
            Console.WriteLine("Attempting to run tests in {0} assemblies", argParser.AssemblyNames.Count());
            runner.Run();

            Console.ForegroundColor = (runner.TotalFailedTests > 0) ? ConsoleColor.Red : ConsoleColor.Green;

            Console.WriteLine("Tests completed: {0}, Passed: {1}, Failed: {2}, Asserts Passed: {3}, Asserts called: {4}",
                runner.TotalTestsRun, runner.TotalPassingTests, runner.TotalFailedTests, runner.AssertionsPassed, runner.AssertionsCalled);

            Console.ResetColor();

            Console.ReadLine();
        }

        static void runner_AssemblySkipped(object sender, AssemblySkippedEventArgs e)
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Skipped {0}", e.AssemblyName);
            Console.ResetColor();
        }

        static void runner_TestCompleted(object sender, TestCompletedEventArgs e)
        {
            if (e.Exception == null)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("{0} test passed", e.TestName);
            }
            else
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("{0} test failed.", e.TestName);
                Console.WriteLine("Exception: {0}", e.Exception.ToString());
                Console.WriteLine();
            }
            Console.ResetColor();
        }
    }
}
