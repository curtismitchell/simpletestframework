# Intro to SimpleTestFramework

## First, why?

There are many featureful frameworks available for .NET: NUnit, MBUnit, MSTest, and xUnit to name a few.  SimpleTestFramework 
was/is not intended to compete with these frameworks.  Instead, SimpleTestFramework was created as a practice project - a Kata
of sorts.  

I was in search of a problem to solve in order to improve my software engineering skills.  During a meeting, I was jotting down notes
 (a practice that is known to get the creative juices flowing) and I ended up sketching out a very basic problem/solution-oriented 
 plan to creating a test framework.  I decided to try and implement it, and this is the result.

## Basic usage

### 1. Setting up your test project

Create a class library project.  You will need to reference two assemblies:

* System.ComponentModel.Composition
* SimpleTestFramework

### 2. Creating a test

1. Create a public class and decorate it with the TestUnit attribute.
2. Create a public parameterless method that returns void, and decorate it with the Behavior attribute.
3. Make assertions using the lone assertion method `Assert.That(bool condition)`.
4. Compile and run from the command line using the SimpleTestFramework.Runner.exe program.

__Check the source for example tests__

### 3. Maybe later

Here are some features I may add while keeping the framework simple:

1. Setup and Teardown methods
2. More command line runner features
3. (Longshot) GUI Integration with VS2010
4. Detect "No Assert Called" within tests and flag them as inconclusive
5. Add how many asserts passed to the results
6. Named asserts - allows the dev to use a test to test several conditions in one method but essentially treat them as separate tests
7. Translate test names to a more readable format e.g. MyHandlerClassTests Should_Handle_Something translates into My Handler Class - Should Handle Something: 
8. Refactor the assert so that it does not throw an exception when it fails.  I want to make sure the whole test runs.