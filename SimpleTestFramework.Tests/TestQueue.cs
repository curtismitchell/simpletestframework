﻿namespace SimpleTestFramework.Tests
{
    using System;
    using System.Linq;
    using SimpleTestFramework;
    using SimpleTestFramework.Extensions;
    using System.Reflection;

    [TestUnit]
    public class TestQueue
    {
        Queue q;

        [BeforeEachTest]
        public void Setup()
        {
            var asm = Assembly.Load(global::SimpleTestFramework.Tests.Properties.Resources.LibraryWithTests);
            q = new Queue(asm);
        }

        [Behavior]
        public void Should_Recognize_Public_Classes_Flagged_With_The_TestUnit_Attribute()
        {
            Assert.That(q.Units.Count() == 2);
        }

        [Behavior]
        public void Should_Enumerate_Tests_In_Each_TestUnit()
        {
            int tests = 0;
            foreach (var u in q.Units)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("{0} has {1} tests", u.GetType().Name, u.Tests().Count()));
                tests += u.Tests().Count();
            }

            Assert.That(2 == tests);
        }
    }
}
