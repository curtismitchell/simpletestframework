﻿namespace SimpleTestFramework.Tests
{
    using System;
    using System.Linq;
    using SimpleTestFramework;

    [TestUnit]
    public class TestAttributes
    {
        [Behavior]
        public void TestUnitAttribute_Should_Be_An_Attribute()
        {
            var tca = new TestUnitAttribute();
            Assert.That(tca is Attribute);
        }

        [Behavior]
        public void TestUnitAttribute_Should_Only_Apply_To_Classes()
        {
            Assert.That(IsValidOn(typeof(TestUnitAttribute), AttributeTargets.Class));
        }

        [Behavior]
        public void BehaviorAttribute_Should_Be_An_Attribute()
        {
            var b = new BehaviorAttribute();
            Assert.That(b is Attribute);
        }

        [Behavior]
        public void BehaviorAttribute_Should_Only_Apply_To_Methods()
        {
            Assert.That(IsValidOn(typeof(BehaviorAttribute), AttributeTargets.Method));
        }

        [Behavior]
        public void BehaviorAttribute_Should_Export_A_Contract_Named_Behavior()
        {
            var attr = new BehaviorAttribute();
            Assert.That("Behavior" == attr.ContractName);
        }

        [Behavior]
        public void BeforeEachTestAttribute_Should_Only_Apply_To_Methods()
        {
            Assert.That(IsValidOn(typeof(BeforeEachTestAttribute), AttributeTargets.Method));
        }

        [Behavior]
        public void BeforeEachTestAttribute_Should_Export_A_Contract_Named_BeforeEachTest()
        {
            var attr = new BeforeEachTestAttribute();
            Assert.That("BeforeEachTest" == attr.ContractName);
        }

        [Behavior]
        public void AfterEachTestAttribute_Should_Only_Apply_To_Methods()
        {
            Assert.That(IsValidOn(typeof(AfterEachTestAttribute), AttributeTargets.Method));
        }

        [Behavior]
        public void AfterEachTestAttribute_Should_Export_A_Contract_Named_BeforeEachTest()
        {
            var attr = new AfterEachTestAttribute();
            Assert.That("AfterEachTest" == attr.ContractName);
        }

        private bool IsValidOn(Type t, AttributeTargets target)
        {
            var usageAttribute = t.GetCustomAttributes(typeof(AttributeUsageAttribute), false).SingleOrDefault();
            return ((usageAttribute as AttributeUsageAttribute).ValidOn == target);
        }
    }
}
