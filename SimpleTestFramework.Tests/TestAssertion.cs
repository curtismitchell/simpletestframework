﻿namespace SimpleTestFramework.Tests
{
    using System;
    using System.Linq;
    using SimpleTestFramework;

    [TestUnit]
    public class TestAssertion
    {
        [Behavior]
        public void Should_Throw_If_Parameter_Is_False()
        {
            try
            {
                Assert.That(false);
            }
            catch (AssertionException)
            {
                Assert.That(true);
            }
        }

        [Behavior]
        public void Assert_Should_Fire_Event_When_Called()
        {
            var eventCalled = false;

            Assert.AssertCalled += ((s, e) =>
            {
                eventCalled = true;
            });

            if(!eventCalled) Assert.That(true);
            Assert.That(eventCalled);
        }

        [Behavior]
        public void AssertionCalledEvent_Should_Return_Status_Of_Assertion()
        {
            var result = true;
            var eventCalled = true;
            Assert.AssertCalled += ((s, e) =>
            {
                eventCalled = true;
                result = e.Passed;
            });

            if (!eventCalled)
                Assert.That(true);
            else
                Assert.That(result == true);
        }
    }
}
